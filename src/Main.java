import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a positive integer : ");
        final long num = sc.nextLong();
        if(num<0) System.out.println("Please enter a positive integer!");
        else System.out.println("Factorial of " + num + " = " + factorialTR(num, 1));
    }

    private static long factorialTR(long num, long sum) {
        if(num==0) return sum;
        return factorialTR(num-1, num * sum);
    }
}
